﻿--select extract ( epoch from (SELECT min(InDate) FROM Tasks));
-- 1389999950
SELECT 	
	extract(epoch from T.InDate) - 1 as submit_time,
	T.TID,
	T.quantum,
	max(U.LoginName),
	max(G.Group_name),
	T.TaskName,
	T.NTime as time_specified, 
	T.NProc as proc_specified,
	---R.RunDate,
	---R.StopDate,
	sum(extract(epoch from (R.StopDate - R.RunDate))/60) as time_executed
FROM Tasks T, Runs R, Users U, Groups G
WHERE 
	U.UserID = T.UserID 
	AND T.TID = R.TID
	AND T.gid = G.gid
	AND T.InDate>'2014-03-18 03:08:01'
	AND T.InDate<'2014-03-19 03:08:01'
GROUP BY t.tid
ORDER BY T.InDate


/*
delete from controls;
delete from dels;
delete from runs;
delete from tasks;
delete from modes;
delete from locks;
delete from sessions;
delete from corrects_of_modes;
delete from ldms;
delete from ldm_tasks;

select * from groups;
*/
/*
insert into groups (gid, group_name, project_name) values (100, 'u0319', 'u0319')
*/